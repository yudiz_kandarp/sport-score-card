import axios from 'axios'
import React, { Suspense } from 'react'
import { useQuery } from 'react-query'
import moment from 'moment'
import { Helmet } from 'react-helmet'
import { Loader } from './Components'
import { Divider } from './Components'
import { LOCALES } from './i18n'
import { useGlobal } from './Context/GlobalContext'
import './App.scss'

const Card = React.lazy(() => import('./Components/Card/Card'))
const CardTitle = React.lazy(() => import('./Components/Card/CardTitle'))
const CardBody = React.lazy(() => import('./Components/Card/CardBody'))

function App() {
	function fetchData() {
		const data = axios.get(
			'https://backend.sports.info/api/v1/nba/game/f34b1dfd-97fd-4942-9c14-05a05eeb5921/summary'
		)
		return data
	}

	const { isLoading, data, isError, error } = useQuery(
		'fetch_data',
		fetchData,
		{
			select: (item) => item?.data.data,
		}
	)

	const date = moment(data?.schedule).format('Do MMMM YYYY')
	const { setLocale } = useGlobal()

	if (isLoading) {
		return <Loader />
	}
	if (isError) {
		return (
			<div className='card_container'>
				<Card>
					<h1>An Error Occured While fetching Data : {error.message}</h1>
				</Card>
				<Helmet>
					<title>Error</title>
				</Helmet>
			</div>
		)
	}

	return (
		<Suspense fallback={<Loader />}>
			<div className='card_container'>
				<Card>
					<CardTitle>
						{`${data?.venue.name}, ${data?.venue.city} | ${date}`}
					</CardTitle>
					<CardBody data={data} />
					<Divider width='100%' height='2px' />
				</Card>

				<Helmet>
					<title>
						{data?.home.alias} VS {data?.away.alias}
					</title>
				</Helmet>
			</div>
			<select
				name='lang'
				id='lang'
				className='language'
				onChange={(e) => setLocale(e.target.value)}
			>
				<option value={LOCALES.ENGLISH}>English</option>
				<option value={LOCALES.FRENCH}>French</option>
				<option value={LOCALES.GERMAN}>German</option>
			</select>
		</Suspense>
	)
}

export default App
