import React from 'react'
import TeamAway from '../../Assets/team_away.svg'
import winnerCup from '../../Assets/winner-cup.svg'
import { finalHighestScore } from '../../Helper/helper'
import { queryClient } from '../../QueryClient/Queryclient'

function AwayTeam() {
	const data = queryClient.getQueryData('fetch_data').data.data
	return (
		<div className='team_away'>
			<img
				className='team_img'
				src={TeamAway}
				alt={data?.away.alias}
				height='30px'
			/>
			<div
				style={{
					display: 'flex',
					alignItems: 'center',
				}}
			>
				{finalHighestScore(data?.home.points, data?.away.points) == 'away' && (
					<img src={winnerCup} alt='win' className='winner_img' height='20px' />
				)}
				<a href='#'>{`${data?.away.market} ${data?.away.name}`} </a>
			</div>
			<p>{data?.away.points}</p>
		</div>
	)
}

export default AwayTeam
