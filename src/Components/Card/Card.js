import React from 'react'
import PropTypes from 'prop-types'
import './card.scss'

function Card({ height, width, children }) {
	return (
		<div style={{ height, width }} className='card'>
			{children}
		</div>
	)
}

Card.propTypes = {
	height: PropTypes.string,
	width: PropTypes.string,
	children: PropTypes.node,
}
export default Card
