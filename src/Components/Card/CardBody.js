import React from 'react'
import HomeTeam from './HomeTeam'
import AwayTeam from './AwayTeam'
import ScoreTable from './ScoreTable'

function CardBody() {
	return (
		<div className='card_body'>
			<HomeTeam />
			<ScoreTable />
			<AwayTeam />
		</div>
	)
}

export default CardBody
