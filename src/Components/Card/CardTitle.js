import React from 'react'
import PropTypes from 'prop-types'

function CardTitle({ children }) {
	return <div className='card_title'>{children}</div>
}

CardTitle.propTypes = {
	children: PropTypes.node,
}
export default CardTitle
