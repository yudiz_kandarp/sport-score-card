import React from 'react'
import TeamHome from '../../Assets/team_home.svg'
import winnerCup from '../../Assets/winner-cup.svg'
import { finalHighestScore } from '../../Helper/helper'
import { queryClient } from '../../QueryClient/Queryclient'

function HomeTeam() {
	const data = queryClient.getQueryData('fetch_data').data.data

	return (
		<div className='team_home'>
			<img
				className='team_img'
				src={TeamHome}
				alt={data?.home.alias}
				height='30px'
			/>
			<div
				style={{
					display: 'flex',
					alignItems: 'center',
				}}
			>
				<a href='#'>{`${data?.home.market} ${data?.home.name}`} </a>
				{finalHighestScore(data?.home.points, data?.away.points) == 'home' && (
					<img src={winnerCup} alt='win' className='winner_img' height='20px' />
				)}
			</div>
			<p>{data?.home.points}</p>
		</div>
	)
}

export default HomeTeam
