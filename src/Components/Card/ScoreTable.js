import React from 'react'
import { finalHighestScore, findGreater } from '../../Helper/helper'
import Translate from '../../i18n/Translate'
import { queryClient } from '../../QueryClient/Queryclient'

function ScoreTable() {
	const data = queryClient.getQueryData('fetch_data').data.data
	return (
		<div className='score_table'>
			<p className='table_title'>{Translate('FINAL')}</p>
			<table className='table'>
				<thead>
					<tr>
						<th></th>
						{data?.home.scoring.map((item) => {
							return <th key={item.number}>{item.number}</th>
						})}
						<th>T</th>
					</tr>
				</thead>

				<tbody>
					<tr>
						<td>{data?.home.alias}</td>
						{data?.home.scoring.map((item, i) => {
							return (
								<td
									key={item.number}
									className={
										findGreater(
											data?.home.scoring.map((item) => item.points),
											data?.away.scoring.map((item) => item.points)
										)[i] >= 0
											? ''
											: 'dark_color'
									}
								>
									{item.points}
								</td>
							)
						})}
						<td
							className={
								finalHighestScore(data?.home.points, data?.away.points) ==
								'home'
									? 'dark_color'
									: ''
							}
						>
							{data?.home.points}
						</td>
					</tr>
					<tr>
						<td>{data?.away.alias}</td>
						{data?.away.scoring.map((item, i) => {
							return (
								<td
									key={item.number}
									className={
										findGreater(
											data?.home.scoring.map((item) => item.points),
											data?.away.scoring.map((item) => item.points)
										)[i] >= 0
											? 'dark_color'
											: ''
									}
								>
									{item.points}
								</td>
							)
						})}
						<td
							className={
								finalHighestScore(data?.home.points, data?.away.points) ==
								'away'
									? 'dark_color'
									: ''
							}
						>
							{data?.away.points}
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	)
}

export default ScoreTable
