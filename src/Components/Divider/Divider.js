import React from 'react'
import PropTypes from 'prop-types'

function Divider({ width = '100%', height = '2px' }) {
	return (
		<div style={{ display: 'flex', width: '100%', justifyContent: 'center' }}>
			<div
				style={{
					height,
					background: '#f2f2f2',
					width,
				}}
			></div>
		</div>
	)
}
Divider.propTypes = {
	width: PropTypes.string,
	height: PropTypes.string,
}
export default Divider
