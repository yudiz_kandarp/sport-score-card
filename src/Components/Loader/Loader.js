import React from 'react'
import './loader.scss'

function Loader() {
	return (
		<div className='loading'>
			<div className='loader'></div>
		</div>
	)
}

export default Loader
