import Divider from './Divider/Divider'
import Loader from './Loader/Loader'

export { Loader, Divider }
