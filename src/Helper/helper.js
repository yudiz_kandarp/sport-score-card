export function findGreater(arr1 = [], arr2 = []) {
	if (arr1.length !== arr2.length) {
		return
	}
	let count = []
	for (let i = 0; i < arr1.length; i++) {
		const el1 = arr1[i]
		const el2 = arr2[i]
		const diff = el2 - el1
		count.push(diff)
	}
	return count
}

export function finalHighestScore(homeScore, awayScore) {
	const greater = homeScore - awayScore
	return greater > 0 ? 'home' : 'away'
}
