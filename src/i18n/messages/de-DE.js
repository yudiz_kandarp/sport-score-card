import { LOCALES } from '../locales'

const data = {
	[LOCALES.GERMAN]: {
		FINAL: 'FINALE',
	},
}
export default data
