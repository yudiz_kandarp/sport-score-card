import { LOCALES } from '../locales'
const data = {
	[LOCALES.FRENCH]: {
		FINAL: 'FINALE',
	},
}
export default data
