import deDE from './de-DE'
import enUS from './en-US'
import frCA from './fr-CA'

const data = { ...deDE, ...enUS, ...frCA }
export default data
